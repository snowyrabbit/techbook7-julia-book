# 『開発者のためのJulia言語入門』 サンプルコード
このレポジトリは技術書典７/8のために執筆した
『開発者のためのJulia言語入門』に記載されたサンプルコードをまとめたものです。

## 使い方
サンプルコードを見たい場合はブラウザ上で対象のファイルをクリックすれば
読み取り専用で表示することができます。

ファイル中のコードをローカルで編集・実行したい場合、
次の手順に従って動作環境をローカルに用意する必要があります。

#### 必要な環境
- [Julia](https://github.com/JuliaLang/julia): v0.7以上必須。v1.0以上を推奨。
- [Jupyter Notebook](https://github.com/jupyter/notebook): ブラウザ上でインタラクティブにコードを実行するためのパッケージ。
- [IJulia](https://github.com/JuliaLang/IJulia.jl): JuliaをJupyter Notebook上で動かすためのパッケージ

#### 環境を用意する
次の手順に従って動作環境を用意します。
##### 1. Juliaをインストール
[公式サイト](https://julialang.org/)からビルド済みバイナリパッケージをインストールします。

CLIでインストールしたい場合はプラットフォームごとのパッケージマネージャーを使うのが手っ取り早いです。
[拙著](https://booth.pm/ja/items/1314345)の「第2章 Juliaをはじめる」に詳細が載っているので参照ください
（試し読み版は無料ダウンロードできます）


##### 2. IJuliaを追加
Julia版JupyterカーネルのIJuliaを追加します。
コマンドラインで`julia`と入力して、JuliaのREPLに入ってから下記のコマンドで`IJulia`を追加します。
```julia
# Julia の REPL で下記を⼊⼒。（注意：Julia のバージョン 0.7 以上必須）
julia> using Pkg
julia> Pkg.add("IJulia")
```

##### 3. Jupayter Notebookをインストール 
Pythonがインストール済みの場合はpipマネージャーを使うのが楽です。

condaを使ってインストールする場合は
[Installing Jupyter Notebook](https://jupyter.readthedocs.io/en/latest/install.html#new-to-python-and-jupyter)
を参照してください。


```python
# Notebookを含めて依存パッケージなど諸々インストール
pip install jupyter
```

###### 4. Jupyter Notebook を起動
ターミナルで次のコマンドでJupyter Notebookを起動します。

ブラウザ上でJupyter Notebookのだダッシュボード画面が開きます。
```
jupyter notebook
```
メニューバーの*File*からクローンしたファイルを選んで開いてください。
編集・実行ができるようになっているはずです。
